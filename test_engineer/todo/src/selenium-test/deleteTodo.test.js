const { Builder, By } = require('selenium-webdriver');

describe('Delete TODO', () => {
  let driver;
  let firstItemText;
  let initItemsCount;

  beforeAll(async () => {
    driver = await new Builder().forBrowser('chrome').build();
    await driver.get('http://localhost:3000');

    let initItems = await driver.findElements(By.css("li.Todo-item"));
    initItemsCount = initItems.length;

    let firstItem = initItems[0];
    firstItemText = await firstItem.findElement(By.tagName("div")).getText();

    let deleteButton = await firstItem.findElement(By.css("button.close"));
    await deleteButton.click();
  });

  afterAll(async () => {
    await driver.quit();
  });

  it('should remove correct TODO item from TODO list', async () => {
    let deletedItem = await driver.findElements(By.xpath(`//li[@class='Todo-item' and div='${firstItemText}']`));
    expect(deletedItem.length).toBe(0);
  });

  it('should remove one TODO item from TODO list', async () => {
    let items = await driver.findElements(By.css("li.Todo-item"));
    let itemsCount = items.length;
    expect(itemsCount).toBe(initItemsCount - 1);
  });
});
