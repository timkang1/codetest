const { Builder, By } = require('selenium-webdriver');

describe('Complete TODO', () => {
  let driver;
  let mark;
  let initItemsCount;
  let clickedItemText;

  beforeAll(async () => {
    driver = await new Builder().forBrowser('chrome').build();
    await driver.get('http://localhost:3000');

    let initItems = await driver.findElements(By.css("li.Todo-item"));
    initItemsCount = initItems.length;

    let undoneTodoDiv = await driver.findElement(By.css("li.Todo-item div.undone"));
    clickedItemText = await undoneTodoDiv.getText();

    let undoneTodoItem = await undoneTodoDiv.findElement(By.xpath("./.."));
    mark = await undoneTodoItem.findElement(By.css("span.icon"));
    await mark.click();
  });

  afterAll(async () => {
    await driver.quit();
  });

  it('should mark TODO item as done', async () => {
    let markAfter = await mark.getText();
    expect(markAfter).toBe("[X]");
  });

  it('should move completed TODO item to bottom of list', async () => {
    let items = await driver.findElements(By.css("li.Todo-item"));
    let lastItemDiv = await items[items.length - 1].findElement(By.tagName("div"));
    let lastItemText = await lastItemDiv.getText();
    expect(lastItemText).toBe(clickedItemText);
  });

  it('should not change number of TODO items', async () => {
    let items = await driver.findElements(By.css("li.Todo-item"));
    let itemsCount = items.length;
    expect(itemsCount).toBe(initItemsCount);
  });
});
