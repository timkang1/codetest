const { Builder, By } = require('selenium-webdriver');

describe('Complete TODO', () => {
  let driver;
  let mark;
  let initItemsCount;
  let clickedItemText;

  beforeAll(async () => {
    driver = await new Builder().forBrowser('chrome').build();
    await driver.get('http://localhost:3000');

    let initItems = await driver.findElements(By.css("li.Todo-item"));
    initItemsCount = initItems.length;

    let doneTodoDiv = await driver.findElement(By.css("li.Todo-item div.done"));
    clickedItemText = await doneTodoDiv.getText();

    let doneTodoItem = await doneTodoDiv.findElement(By.xpath("./.."));
    mark = await doneTodoItem.findElement(By.css("span.icon"));
    await mark.click();
  });

  afterAll(async () => {
    await driver.quit();
  });

  it('should mark TODO item as undone', async () => {
    let markAfter = await mark.getText();
    expect(markAfter).toBe("[ ]");
  });

  it('should move uncompleted TODO item to top of list', async () => {
    let items = await driver.findElements(By.css("li.Todo-item"));
    let firstItemDiv = await items[0].findElement(By.tagName("div"));
    let firstItemText = await firstItemDiv.getText();
    expect(firstItemText).toBe(clickedItemText);
  });

  it('should not change number of TODO items', async () => {
    let items = await driver.findElements(By.css("li.Todo-item"));
    let itemsCount = items.length;
    expect(itemsCount).toBe(initItemsCount);
  });
});
