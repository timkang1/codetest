const { Builder, By } = require('selenium-webdriver');

describe('Create TODO', () => {
  let driver;
  let inputItem = "Get 1 Boba!";
  let initItemsCount;

  beforeAll(async () => {
    driver = await new Builder().forBrowser('chrome').build();
    await driver.get('http://localhost:3000');

    let initItems = await driver.findElements(By.css("li.Todo-item"));
    initItemsCount = initItems.length;

    let form = await driver.findElement(By.css("form.form-inline"));

    let input = await form.findElement(By.css("input.form-control"));
    await input.sendKeys(inputItem);

    let createButton = await form.findElement(By.css("button.submit"));
    await createButton.click();
  });

  afterAll(async () => {
    await driver.quit();
  });

  it('should add TODO item with entered text', async () => {
    let searchEnteredItem = await driver.findElements(By.xpath(`//li[@class='Todo-item' and div='${inputItem}']`));
    expect(searchEnteredItem.length).toBe(1);
  })

  it('should add one TODO item to TODO list', async () => {
    let items = await driver.findElements(By.css("li.Todo-item"));
    let itemsCount = items.length;
    expect(itemsCount).toBe(initItemsCount + 1);
  });

  it('should prepend TODO item to top of TODO list', async () => {
    let items = await driver.findElements(By.css("li.Todo-item"));
    let firstItemDiv = await items[0].findElement(By.tagName("div"));
    let firstItemText = await firstItemDiv.getText();
    expect(firstItemText).toBe(inputItem);
  });

  it('should create an undone TODO item', async() => {
    let items = await driver.findElements(By.css("li.Todo-item"));
    let firstItemIcon = await items[0].findElement(By.css("span.icon"));
    let firstItemMark = await firstItemIcon.getText()
    expect(firstItemMark).toBe("[ ]");
  });
});
