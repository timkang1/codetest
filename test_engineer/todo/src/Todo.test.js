import React from 'react';
import ReactDOM from 'react-dom';
import Todos from './Todo';
import { mount } from './enzyme';

describe.skip('Todo', () => {

  let wrapper;

  beforeEach(() => {
    const todoItems = [
      { index: 1, value: "learn react", done: false },
      { index: 2, value: "Go shopping", done: true },
      { index: 3, value: "buy flowers", done: true },
    ];
    wrapper = mount(<Todos initItems={todoItems} />);
  });

  describe('renders components', () => {
    it('renders "Todo list" header', () => {
      expect(wrapper.containsMatchingElement(<h1 className="Todo-header">Todo list</h1>)).toBe(true);
    });

    it('renders TodoList `ul` element', () => {
      expect(wrapper.find('ul.Todo-group').length).toBe(1);
    });

    it('renders correct number of TodoListItem `li` elements', () => {
      expect(wrapper.find('li.Todo-item').length).toBe(3);
    });

    it('renders "[  ]" on undone TodoListItem', () => {
      const undoneTodoItem = wrapper.find('.undone').first().closest('.Todo-item');
      const undoneMark = undoneTodoItem.find('.icon').text();
      expect(undoneMark).toBe("[  ]");
    });

    it('renders "[X]" on done TodoListItem', () => {
      const doneTodoItem = wrapper.find('.done').first().closest('.Todo-item');
      const doneMark = doneTodoItem.find('.icon').text();
      expect(doneMark).toBe("[X]");
    });

    it('renders TodoForm input field', () => {
      const todoForm = wrapper.find('TodoForm');
      expect(todoForm.containsMatchingElement(<input />)).toBe(true);
    });

    it('renders TodoForm submit-type button', () => {
      const todoForm = wrapper.find('TodoForm');
      expect(todoForm.containsMatchingElement(<button type="submit">+</button>)).toBe(true);
    });
  });

  describe('Create Functionality', () => {
    it('should prevent adding blank TODO', () => {
      const form = wrapper.find('TodoForm');
      const button = form.find('button[className="submit"]');
      button.simulate('submit');
      expect(wrapper.find('.Todo-item').length).toBe(3);
    });

    describe('populate input and click "add"', () => {
      const item = "Get Boba!";

      beforeEach(() => {;
        const form = wrapper.find('TodoForm');
        const input = form.find('input[className="form-control"]');
        input.instance().value = item;
        const button = form.find('button[className="submit"]');
        button.simulate('submit');
      });

      it('should add item to state', () => {
        const items = wrapper.state().todoItems;
        let found = false;
        for (let i = 0; i < items.length; i++) {
          if (items[i].value == item) {
            found = true;
            break;
          }
        }
        expect(found).toBe(true);
      });

      it('should render the added item on top of TODO list', () => {
        const topItem = wrapper.find('TodoListItem').first();
        expect(topItem.find('div').text()).toBe(item);
      });

      it('should mark the added item as undone', () => {
        const topItem = wrapper.find('TodoListItem').first();
        expect(topItem.find('span[className="icon"]').text()).toBe("[  ]");
      });

      it('should clear the input field', () => {
        const input = wrapper.find('input[className="form-control"]');
        expect(input.instance().value).toBe("");
      });
    });
  });

  describe('Complete Functionality', () => {
    describe('click on "[  ]" mark of incomplete item', () => {

      let clickedTaskName;
      let mark;

      beforeEach(() => {
        const undoneItem = wrapper.find('.undone').first().closest('.Todo-item');
        clickedTaskName = undoneItem.find('div').text();
        mark = undoneItem.find('.icon');
        mark.simulate('click');
      });

      it('should switch undone item to done', () => {
        expect(mark.text()).toBe("[X]");
      });

      it('should move item to bottom of list', () => {
        const bottomItem = wrapper.find('TodoListItem').last();
        const bottomItemName = bottomItem.find('div').text();
        expect(bottomItemName).toBe(clickedTaskName);
      });

      it('should render same number of items', () => {
        expect(wrapper.find('TodoListItem').length).toBe(3);
      });
    });
  });

  describe('Un-Complete Functionality', () => {
    describe('click on "[X]" mark of complete item', () => {

      let mark;
      let clickedTaskName;

      beforeEach(() => {
        const doneItem = wrapper.find('.done').first().closest('.Todo-item');
        clickedTaskName = doneItem.find('div').text();
        mark = doneItem.find('.icon');
        mark.simulate('click');
      });

      it('should switch done item to undone', () => {
        expect(mark.text()).toBe("[  ]");
      });

      it('should move item to top of list', () => {
        const topItem = wrapper.find('TodoListItem').first();
        const topItemName = topItem.find('div').text();
        expect(topItemName).toBe(clickedTaskName);
      });

      it('should render same number of items', () => {
        expect(wrapper.find('TodoListItem').length).toBe(3);
      });
    });
  });

  describe('Destroy Functionality', () => {
    describe('click on delete button of item', () => {

      let targetTaskName;

      beforeEach(() => {
        const targetItem = wrapper.find('TodoListItem').first();
        targetTaskName = targetItem.find('div').text();
        const deleteButton = targetItem.find('button[className="close"]');
        deleteButton.simulate('click');
      });

      it('should remove item from state', () => {
        const items = wrapper.state().todoItems;
        let found = false;
        for (let i = 0; i < items.length; i++) {
          if (items[i].value == targetTaskName) {
            found = true;
            break;
          }
        }
        expect(found).toBe(false);
      });

      it('should remove one item from state', () => {
        const items = wrapper.state().todoItems;
        expect(items.length).toBe(2);
      });

      it('should remove correct item from list', () => {
        expect(wrapper.containsMatchingElement(<div>{targetTaskName}</div>)).toBe(false);
      });

      it('should remove one item from list', () => {
        expect(wrapper.find('TodoListItem').length).toBe(2);
      });
    });
  });
});
